#$HOME/.bash_functions

# Extract
extract()
{
    if [ -f "$1" ]; then
        case "$1" in
            *.7z)      7z x "$1"       ;;
            *.Z)       uncompress "$1" ;;
            *.bz2)     bunzip2 "$1"    ;;
            *.gz)      gunzip "$1"     ;;
            *.jar)     jar xf "$1"     ;;
            *.rar)     rar x "$1"      ;;
            *.tar)     tar xvf "$1"    ;;
            *.tar.bz2) tar xvjf "$1"   ;;
            *.tar.gz)  tar xvzf "$1"   ;;
            *.tbz2)    tar xvjf "$1"   ;;
            *.tgz)     tar xvzf "$1"   ;;
            *.zip)     unzip "$1"      ;;
            *)         echo "Unable to extract '$1'" ;;
        esac
    else
        echo "'$1' not found!"
    fi
}

# Content
co()
{
    cat $@ | pbcopy
}

# Content
cs()
{
    cat ${@:2} | grep -i "$1"
}

# Find
ff()
{
    find . -iname "*$1*" | grep -i "$1"
}

# Find
fd()
{
    find . -type d -iname "*$1*" | grep -i "$1"
}

# Find
fe()
{
    find . -iname "*$1*" -print -exec ${@:2} {} \; | grep -i "$1"
}

# Find
fr()
{
    find . -iname "*$1*" -delete -print | grep -i "$1"
}

# History
hs()
{
    if [ $# -eq 0 ]; then
        history | less +G
    else
        history | grep --color=always "$1" | less +G
    fi
}

# List
le()
{
    find . -type f -name "*.*" | awk -F. '{print $NF}' | sort -u
}

# Make
mkcd()
{
    mkdir -p $@ && cd ${@:$#}
}

# Permission
pm()
{
    stat -f '%A %N' $@
}

# Python
pycl()
{
    find . -type f -name "*.py[co]" -delete -o -type d -name __pycache__ -delete
}

# Reload
rl()
{
    if [ -f $HOME/.bash_profile ]; then
        source $HOME/.bash_profile
    elif [ -f $HOME/.bashrc ]; then
        source $HOME/.bashrc
    fi
}

# Upgrade
up()
{
    brew -v update && brew -v upgrade && brew cleanup && brew doctor
    sudo PIP_REQUIRE_VIRTUALENV=false pip3 install -U pip setuptools requests virtualenv
}

# Virtualenv
vr()
{
    local VR=$(dirname $VIRTUAL_ENV)/requirements.txt

    ! [ -z $VIRTUAL_ENV ] && [ -f $VR ] && pip install -r $VR
}

# Virtualenv
vv()
{
    if [ -z $VIRTUAL_ENV ]; then
        ! [ -d venv ] && virtualenv venv
        source venv/bin/activate
        [ -f requirements.txt ] && pip install -r requirements.txt
    else
        deactivate
    fi
}

# Work
ws()
{
    [ -f $WORK/.workrc ] && source $WORK/.workrc && cd $WORK
}

# Zip
zp()
{
    zip -r "${1%%/}.zip" "$1"
}
