#$HOME/.bash_aliases

# Navigation

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias -- -='cd -'

alias de='cd $HOME/Desktop'

# Listing

alias ls='ls -Gh'
alias la='ls -a'

alias l='ls -CF'
alias ll='ls -aFl'
alias lr='ls -aFR1'
alias lt='ls -aFlt'

alias l1='ls -1'
alias l2='la -1'

alias l.='ls -ad .*'

alias lm='CLICOLOR_FORCE=1 lt | head'

# Cleaning

alias cl='clear'
alias hc='rm -rf $HOME/.bash_history $HOME/.bash_sessions ; history -c'

# Command

alias h='history | less +G'
alias x='chmod +x'

alias du='du -hm'
alias rf='rm -rf'
alias rm='rm -i'
alias rp='rsync -Prv -hh'
alias sl='sleep'

alias pbc='pbcopy'
alias pbv='pbpaste'
alias ppd='popd'
alias psd='pushd .'
alias src='source'

alias grep='grep --color=auto'
alias less='less -FRX'
alias mkdir='mkdir -p'
alias tree='tree -Chs'

# Python

alias py='python'
alias py3='python3'

alias pp='pip'

alias spp='sudo PIP_REQUIRE_VIRTUALENV=false pip'
alias spp3='sudo PIP_REQUIRE_VIRTUALENV=false pip3'

alias spcl='sudo rm -rf $HOME/Library/Caches/pip'

# Tree

alias tr='tree'

alias ta='tree -a'
alias tl='tree -L'

alias tal='tree -aL'

# Docker

alias dki='docker images'
alias dkia='docker images -a'

alias dkps='docker ps'
alias dkpsa='docker ps -a'

alias dkrac='docker rm $(docker ps -a -q)'
alias dkrai='docker rmi $(docker images -a -q)'

alias dksac='docker stop $(docker ps -a -q)'

alias dkspa='docker system prune -a'

# macOS

alias dscl='find . -type f -name .DS_Store -delete -print | grep .DS_Store'

# Brew

alias br='brew'

alias bcl='brew cleanup'
alias bdo='brew doctor'
alias bin='brew -v install'
alias bss='brew search'
alias bud='brew -v update'
alias bug='brew -v upgrade'
alias bup='brew -v update && brew -v upgrade'
