#$HOME/.bashrc

# Locale
export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=pt_BR.UTF-8

# Configuration
bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'

# Editor
export EDITOR=nano
export VISUAL=nano

# History
export HISTCONTROL=ignoreboth
export HISTIGNORE='h:hs*'
export HISTSIZE=5000

# Homebrew
if [ -f /opt/homebrew/bin/brew ]; then
    eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Add personal bin to $PATH
if [ -d $HOME/.bin ]; then
    export PATH=$HOME/.bin:$PATH
fi

# Enable bash completion
if [ -f /opt/homebrew/etc/bash_completion ]; then
    source /opt/homebrew/etc/bash_completion
fi

# Load $HOME/.bash_aliases
if [ -f $HOME/.bash_aliases ]; then
    source $HOME/.bash_aliases
fi

# Load $HOME/.bash_functions
if [ -f $HOME/.bash_functions ]; then
    source $HOME/.bash_functions
fi

# Load $HOME/.fzf
if [ -f $HOME/.fzf ]; then
    source $HOME/.fzf
fi

# Set $WORK as $HOME/Work
if [ -d $HOME/Work ]; then
    export WORK=$HOME/Work
fi

# Require active virtualenv to run pip
export PIP_REQUIRE_VIRTUALENV=true
